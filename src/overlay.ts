import * as PIXI from 'pixi.js';


interface PixiNode {
    child: PIXI.Sprite;
    latLng: google.maps.LatLng;
}
export class PixiOverlay {

    requestAnimationFrameId: any;
    topLeft: google.maps.LatLng;
    render: PIXI.WebGLRenderer | PIXI.CanvasRenderer;
    stage: PIXI.Container;
    map: google.maps.Map;
    overlay: google.maps.OverlayView;

    nodes: PixiNode[] = [];
    initZoom: number;
    initOrigin: google.maps.LatLng;
    scale: number;

    currentScale: number;
    targetScale: number;

    zoom = {
        /** Target to get to */
        target: 0,
        /** Last value set  */
        last: 0,
        /** Current zoom level */
        current: 0,
        /** time that the zoom animation started */
        start: 0
    }

    constructor(map: google.maps.Map, overlay: google.maps.OverlayView) {
        this.map = map;
        map.setOptions({
            animatedZoom: false
        })
        this.overlay = overlay;

        this.overlay.onAdd = this.onAdd.bind(this);
        this.overlay.onRemove = this.onRemove.bind(this);
        this.overlay.draw = this.onDraw.bind(this);

        this.stage = new PIXI.Container();
        this.render = PIXI.autoDetectRenderer(512, 512, { transparent: true });

        this.initZoom = 10;
        this.initOrigin = new google.maps.LatLng(0, 0);
        this.scale = map.getZoom()
        this.zoom.current = 0;
        this.zoom.target = map.getZoom();
        this.overlay.setMap(this.map);
    }

    onAdd() {
        console.log('Added');
        this.render.view.style.outline = '5px solid red';
        this.render.view.style.position = 'absolute';
        const div = document.createElement('div')
        div.style.position = 'absolute';
        div.style.top = '0px';
        div.style.left = '0px';
        div.appendChild(this.render.view);
        this.overlay.getPanes().overlayMouseTarget.appendChild(div);

        // google.maps.event.addListener(this.map, 'resize', this.onResize.bind(this));
        google.maps.event.addListener(this.map, 'bounds_changed', this.onDraw.bind(this));
        google.maps.event.addListener(this.map, 'zoom_changed', this.onZoom.bind(this))

        const projection = this.map.getProjection()
        for (const node of this.nodes) {
            const point = projection.fromLatLngToPoint(node.latLng);

            node.child.anchor.x = 0.5;
            node.child.anchor.y = 0.5;
            node.child.x = point.x;
            node.child.y = point.y;
            node.child.tint = 0xff0000;
            node.child.interactive = true;
            node.child.on('click', (e: PIXI.interaction.InteractionEvent) => {
                console.log('click', e, e.stopPropagation(), e.target)
                this.map.panTo(node.latLng);

            });
            node.child.on('mouseover', () => node.child.tint = 0x000000)
            node.child.on('mouseout', () => node.child.tint = 0xff0000)

            console.log('AddPoint', `${node.child.x},${node.child.y}`)
            this.stage.addChild(node.child);
            this.stage.on('mouseover', () => console.log('mouseover'));
        }

        this.onResize();
        this.onDraw();

    }
    addAt(child: any, lat: number, lng: number) {
        this.nodes.push({
            child,
            latLng: new google.maps.LatLng(lat, lng)
        });
    }

    onRemove() {

    }
    onZoom() {
        console.log('ZoomChanged', this.map.getZoom(), 'current', this.zoom.current, 'target', this.zoom.target)
        this.zoom.start = Date.now();
        this.zoom.target = this.map.getZoom();
    }

    onRender() {
        this.requestAnimationFrameId = null;
        this.render.render(this.stage);
    }


    onDraw() {
        // topLeft can't be calculated from map.getBounds(), because bounds are
        // clamped to -180 and 180 when completely zoomed out. Instead, calculate
        // left as an offset from the center, which is an unwrapped LatLng.
        // const bounds = this.map.getBounds();
        // console.log('Bounds', bounds.toJSON());
        const top = this.map.getBounds().getNorthEast().lat();
        const center = this.map.getCenter();
        const currentZoom = this.map.getZoom();
        const scale = Math.pow(2, currentZoom);
        const left = center.lng() - (this.render.width * 180) / (256 * scale);
        this.topLeft = new google.maps.LatLng(top, left);
        const projection = this.overlay.getProjection();

        // Canvas position relative to draggable map's container depends on
        // overlayView's projection, not the map's. Have to use the center of the
        // map for this, not the top left, for the same reason as above.
        const divCenter = projection.fromLatLngToDivPixel(center);
        const offsetX = -Math.floor(this.render.width / 2 - divCenter.x);
        const offsetY = -Math.floor(this.render.height / 2 - divCenter.y);

        // console.log(divCenter);

        // const zoomDiff = this.zoom.target - this.zoom.last;
        // console.log('Move', Math.floor(sw.x), Math.floor(ne.y), '@', zoomDiff);

        this.render.view.style.transform = 'translate(' + offsetX + 'px,' + offsetY + 'px)'

        const mapTopLeft = this.map.getProjection().fromLatLngToPoint(this.topLeft)
        // console.log('TopLeft', mapTopLeft.x, mapTopLeft.y);
        this.stage.position.x = -mapTopLeft.x * scale;
        this.stage.position.y = -mapTopLeft.y * scale;

        console.log('Scale', scale);

        // let nodeScale = 0;
        // if (this.zoom.current === 0) {
        //     nodeScale = this.zoom.last = this.zoom.current = this.zoom.target;
        // } else if (zoomDiff > 0) {
        //     nodeScale = this.zoom.current = this.zoom.current + zoomDiff * 0.1;
        // }

        // if (this.zoom.current > this.zoom.target) {
        //     this.zoom.last = this.zoom.current = this.zoom.target;
        //     nodeScale = this.zoom.target;
        // }
        const invScale = 1 / scale;

        if (Math.abs(divCenter.x) > 1) {
            for (const node of this.nodes) {
                node.child.visible = false;
            }
        } else {
            for (const node of this.nodes) {
                node.child.scale.set(invScale, invScale);
                node.child.visible = true;
            }
        }
        this.stage.scale.set(scale, scale);
        this.zoom.last = this.map.getZoom();

        this.render.render(this.stage);


    }

    onResize() {
        const mapDiv = this.map.getDiv();
        const width = mapDiv.offsetWidth;
        const height = mapDiv.offsetHeight;
        console.log('resize', width, height);
        this.render.resize(width, height);
        this.scheduleUpdate();
    }


    scheduleUpdate() {
        // if (this.requestAnimationFrameId == null) {
        //     this.requestAnimationFrameId =
        //         requestAnimationFrame(this.onRender.bind(this));
        // }
    }
}
