import { PixiOverlay } from "./overlay";
import * as PIXI from 'pixi.js';

function start() {
    const el = document.getElementById('map');
    const mapOptions = {
        center: {
            lat: -41.2786889,
            lng: 174.7768529,
        },
        zoom: 10,
        /* Disable clicking points of interest. This makes it confusing when trying to click on collections. */
        clickableIcons: false,
        /* Disable 45° camera angle in satellite map mode. Only top-down angle will position collections accurately. */
        tilt: 0,
        /* Feature works but is not tested. Disable until asked for. */
        rotateControl: false
    };
    const map = new google.maps.Map(el, mapOptions);

    const overlay = new PixiOverlay(map, new google.maps.OverlayView());
    // const stage = new PIXI.Container();
    // const render = PIXI.autoDetectRenderer(512, 512, { transparent: false, backgroundColor: 0xfafafa });
    // document.getElementById('map').appendChild(render.view);

    const texture = PIXI.utils.TextureCache['./dist/bunny.png'];

    for (let x = -5; x <= 10; x += 5) {
        for (let y = -5; y <= 10; y += 5) {
            // const texture = PIXI.utils.TextureCache['./bunny.png'];
            const sprite = new PIXI.Sprite(texture);


            sprite.x = x;
            sprite.y = y;

            overlay.addAt(sprite, x, y);
        }
    }

    const sprite = new PIXI.Sprite(texture);
    sprite.x = -41.2786889;;
    sprite.y = 174.7768529;
    overlay.addAt(sprite, sprite.x, sprite.y);

    // render.render(stage);
}


function waitForGoogle() {
    if (typeof google === 'undefined') {
        return setTimeout(waitForGoogle, 10);
    }

    console.log('Loaded...')

    return start()
}

PIXI.loader
    .add('./dist/bunny.png')
    .load(waitForGoogle)
// document.addEventListener('DOMContentLoaded', waitForGoogle);
